# PyCAHusk - a collection of image processing and shape analysis applications

This repository exists as a wrapper around the various libraries used in
conjunction with [PyCA][PyCA].

## Installation

First clone this repository, using the `--recursive` option:
```
git clone git@bitbucket.org:scicompanat/pycahusk.git --recursive
```
If you've already cloned and forgot to use the `--recursive` option, then run
the following command to initialize all submodules:
```
git submodule update --init --recursive
```
In the future, to fetch the latest stable changes to all the applications
contained in PyCAHusk, you can do
```
git pull
git submodule update --recursive
```

### Building

PyCAHusk clones the main [PyCA][PyCA] repository into a subdirectory named
`.pyca_source`, which includes C++ code that needs to be compiled.  The steps
for building the library in a default configuration are provided in the script
`build.sh`.  So for the default build you can issue
```
./build.sh
```
This will create the `.pyca_build` directory.

_Note_ that `build.sh` will need to be run again whenever new changes are
fetched using the commands above.

If you'd like to enable specific options and create a custom build, you can
simply run `ccmake .` from within the `.pyca_build` directory, followed by
`make`.

[PyCA]: http://bitbucket.org/scicompanat/pyca
