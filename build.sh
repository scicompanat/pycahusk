#!/bin/sh

HUSKDIR=$(readlink -f $(dirname $0))

PYCASRC=$HUSKDIR/.pyca_source
PYCABIN=$HUSKDIR/.pyca_build

CMAKEFLAGS=""

mkdir -p $PYCABIN
cd $PYCABIN
cmake -D CMAKE_BUILD_TYPE=Release \
      -D CUDA_NVCC_FLAGS_RELEASE=--pre-include\ $PYCASRC/preinc.h $PYCASRC
$CMD
make
cd -
